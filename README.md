# README #

Insanity Jam Game Idea Generator   
Version 0.87

Hosted at [https://alamantus.gitlab.io/GameIdeaGenerator](https://alamantus.gitlab.io/GameIdeaGenerator)

Takes a seed and randomly generates a sentence by selecting random words from several word lists and by randomly selecting a sentence structure to place the words into.
This version splits the process into helper functions, which makes it easier to add more sentence structures in the future.

###Dependencies###

* seedrandom.js
* jquery-1.x.js  (Any jquery, basically. If unchanged, this folder uses jquery-1.11.0.js or jquery-1.11.0.min.js)   
* values/gametypes.txt
* values/nouns.txt
* values/pluralnouns.txt
* values/concepts.txt
* values/adjectives.txt
* values/locations.txt
* values/descriptions.txt
* values/verbs2nd.txt
* values/verbs2ndconcepts.txt
* values/verbs3rd.txt
* values/additions.txt

### Contribution guidelines ###

* Leave Issues in the issue tracker
* Leave Ideas in the issue tracker

Author: Robbie Antenesse  
Contact: dev@alamantus.com